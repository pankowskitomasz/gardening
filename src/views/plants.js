import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import PlantsS1 from "../components/plants-s1";
import PlantsS2 from "../components/plants-s2";
import PlantsS3 from "../components/plants-s3";

class Plants extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <PlantsS1/>
                <PlantsS2/>
                <PlantsS3/>
            </Container>    
        );
    }
}

export default Plants;