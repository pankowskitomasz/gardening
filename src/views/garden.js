import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import GardenS1 from "../components/garden-s1";
import GardenS2 from "../components/garden-s2";
import GardenS3 from "../components/garden-s3";

class Garden extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <GardenS1/>
                <GardenS2/>
                <GardenS3/>
            </Container>    
        );
    }
}

export default Garden;